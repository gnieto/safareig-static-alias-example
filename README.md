# Static alias 

This sample project demonstrate how to use Safareig modules to serve a partial part of the Matrix spec. Specifically, it loads the data from a TOML file and can be obtained either via the client api or the federation api.

For the federation part, it automatically creates all the required keys in order to fulfill the federation protocol.


## Set-up

Currently, the project requires to set up a few modules:

- CoreModule: Provides a minimal set of services that most of the other modules requires. It contains services related to config loading or similar
- ServerKeysModule: Handles federation keys and enables access via federation api
- AliasesModule: Contains the alias module features, as defined in the matrix spec. It contains both client and federation endpoints and core services for the feature.
- StaticAliasModule: This is the module which provides the overrides of some interfaces. For example, we are replaicing the storage trait in order to provide a list of alias read from a TOML file instead of reading it from sqlite.
- AxumModule: Contains modules related to the Axum integration of the project, such as authorization.

## Limitations

Right now, sql migrations are not bundled in Safareig's crates, which means that the migration scripts needs to be copied from the source repository.

Also, it has some dependencies with some core services (DeviceStorage/TokenHandler/AppserviceRepository) which should be optional.