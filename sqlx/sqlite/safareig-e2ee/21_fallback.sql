CREATE TABLE IF NOT EXISTS fallback_keys (
    device_id       STRING      NOT NULL,
    algorithm       STRING      NOT NULL,
    otk_blob        JSON        NOT NULL,
    key_name        STRING,
    used            NUMBER      DEFAULT 0,

    PRIMARY KEY (device_id, algorithm)
);