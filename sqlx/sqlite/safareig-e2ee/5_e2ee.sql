CREATE TABLE IF NOT EXISTS device_keys (
    user_id         STRING      NOT NULL,
    device_id       STRING      NOT NULL,
    key_blob        JSON        NOT NULL,

    PRIMARY KEY (user_id, device_id)
);

CREATE TABLE IF NOT EXISTS device_otk (
    device_id       STRING      NOT NULL,
    algorithm       STRING      NOT NULL,
    fallback        NUMBER      NOT NULL,
    otk_blob        JSON        NOT NULL
);