CREATE TABLE IF NOT EXISTS notifications_tmp (
    user_id         STRING NOT NULL,
    event_id        STRING NOT NULL,
    room_id         STRING NOT NULL,
    stream_token    NUMBER NOT NULL,
    push_rule       STRING NOT NULL,
    flags           NUMBER NOT NULL,
    insertion       timestamp default (strftime('%s', 'now')),

    PRIMARY KEY(user_id, stream_token)
);

INSERT INTO notifications_tmp(user_id, event_id, room_id, stream_token, push_rule, flags)
SELECT * FROM notifications;

DROP TABLE IF EXISTS notifications;
ALTER TABLE notifications_tmp RENAME TO notifications;