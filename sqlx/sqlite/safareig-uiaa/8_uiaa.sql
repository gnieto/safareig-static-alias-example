CREATE TABLE IF NOT EXISTS sessions (
    -- TODO: Maybe this needs to be split in order to make it more relational
    session_id          STRING      PRIMARY KEY NOT NULL,
    data                BLOB,
    last_update         TIME
);