CREATE TABLE IF NOT EXISTS devices
(
    id              STRING      NOT NULL,
    user_id         STRING      NOT NULL,
    token           STRING      DEFAULT NULL,
    display_name    STRING      DEFAULT NULL,
    last_seen_ip    STRING      DEFAULT NULL,
    last_seen_ts    INTEGER     DEFAULT NULL,

    PRIMARY KEY(id, user_id)
);

CREATE TABLE IF NOT EXISTS device_list
(
    user_id         STRING      NOT NULL PRIMARY KEY,
    stream_id       INTEGER     NOT NULL,
    stale           BOOLEAN
);
