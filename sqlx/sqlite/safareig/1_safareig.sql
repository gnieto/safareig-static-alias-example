CREATE TABLE IF NOT EXISTS users
(
    id          STRING      PRIMARY KEY NOT NULL,
    avatar      STRING      DEFAULT NULL,
    password    STRING      DEFAULT NULL,
    display     STRING      DEFAULT NULL
);

-- TODO: Change most of the string types to internalized strings -> int
CREATE TABLE IF NOT EXISTS events (
    event_id        STRING      PRIMARY KEY NOT NULL,
    event_type      STRING      NOT NULL, -- TODO: Change this to int when internalized strings v2 are ready
    depth           INTEGER,     
    room_id         STRING      NOT NULL, -- TODO: Change this to int when internalized strings v2 are ready
    sender          STRING      NOT NULL, -- TODO: Change this to int when internalized strings v2 are ready
    event_data      BLOB        NOT NULL,
    transaction_id  STRING
);

CREATE TABLE IF NOT EXISTS event_state (
    -- We do not set a foreign key to event id because the event may not exist yet
    event_id        STRING      PRIMARY KEY NOT NULL,
    state_id        BLOB        NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS events_txn_id
    ON events (transaction_id);

CREATE TABLE IF NOT EXISTS event_stream (
    stream_token    INTEGER     PRIMARY KEY AUTOINCREMENT,
    event_id        STRING      NOT NULL,

    FOREIGN KEY(event_id) REFERENCES events(event_id)  
);

CREATE TABLE IF NOT EXISTS backward_extremities (
    event_id            STRING      NOT NULL,
    room_id             STRING      NOT NULL,
    topological_depth   NUMERIC,
    topological_sub     NUMERIC   
);

CREATE TABLE IF NOT EXISTS forward_extremities (
    event_id            STRING      NOT NULL,
    sha256              STRING,
    depth               NUMERIC,
    state               BLOB,    

    FOREIGN KEY(event_id) REFERENCES events(event_id)  
);

CREATE TABLE IF NOT EXISTS checkpoints (
    worker_id           STRING      PRIMARY KEY NOT NULL,
    checkpoint_data     TEXT 
);

CREATE TABLE IF NOT EXISTS servers_rooms (
    server_name         STRING      NOT NULL,
    room_id             STRING      NOT NULL
);

CREATE TABLE IF NOT EXISTS user_filters (
    -- TODO: Maybe I could code the filter id as an integer/uuid, but this will require changes in the code
    filter_id           STRING      PRIMARY KEY NOT NULL,
    filter_data         BLOB        NOT NULL
);

CREATE TABLE IF NOT EXISTS user_memberships (
    user_id             STRING      NOT NULL,
    room_id             STRING      NOT NULL,
    state               STRING      NOT NULL,
    stream_token        INTEGER,     
    event_id            STRING      NOT NULL,

    PRIMARY KEY(user_id, room_id),
    FOREIGN KEY(event_id) REFERENCES events(event_id)
);

CREATE TABLE IF NOT EXISTS room_aliases (
    room_alias          STRING      PRIMARY KEY NOT NULL,
    room_id             STRING      NOT NULL,
    creator             STRING
);

CREATE TABLE IF NOT EXISTS states (
    state_id            BLOB        NOT NULL,
    event_type          BLOB        NOT NULL,
    state_key           BLOB,
    event_id            STRING      NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS states_unique_key
    ON states (state_id, event_type, state_key);

CREATE TABLE IF NOT EXISTS internalized_strings (
    int_string              STRING  PRIMARY KEY NOT NULL,
    int_id                  BLOB    NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_internalized
    ON internalized_strings (int_id);


CREATE TABLE IF NOT EXISTS rooms
(
    room_id         STRING      NOT NULL PRIMARY KEY,
    event_encoding  STRING      NOT NULL,
    room_version    STRING      NOT NULL
);

CREATE TABLE IF NOT EXISTS presence (
    user_id         STRING      NOT NULL PRIMARY KEY,
    state           STRING      NOT NULL,
    message         STRING,
    active          TIME,
    passive         TIME
);


-- TODO: Maybe split each rule into a single row
CREATE TABLE IF NOT EXISTS push_rules (
    user_id         STRING      NOT NULL PRIMARY KEY,
    version         NUMBER,
    rules           JSON,

    FOREIGN KEY(user_id) REFERENCES users(id) 
);

CREATE TABLE IF NOT EXISTS pushers (
    user_id             STRING      NOT NULL PRIMARY KEY,
    pushkey             STRING      NOT NULL,
    -- TODO: Pusher Type
    app_id              STRING      NOT NULL,
    app_display_name    STRING      NOT NULL,
    device_display_name STRING      NOT NULL,
    profile_tag         STRING,
    lang                STRING,
    pusher_data         JSON,

    FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS to_device_events (
    device_id       STRING NOT NULL,
    event_type      STRING NOT NULL,
    content         JSON NOT NULL,
    sender          STRING NOT NULL,
    message_id      STRING NOT NULL,
    stream_token    NUMBER NOT NULL
);

CREATE TABLE IF NOT EXISTS invites (
    user_id         STRING NOT NULL,
    stream_token    NUMBER NOT NULL,
    room_id         STRING NOT NULL,
    event_id        STRING,
    room_version    STRING,
    state           JSON,

    PRIMARY KEY (user_id, room_id)
);

CREATE TABLE IF NOT EXISTS notifications (
    user_id         STRING NOT NULL,
    event_id        STRING NOT NULL,
    room_id         STRING NOT NULL,
    stream_token    NUMBER NOT NULL,
    push_rule       STRING NOT NULL,
    flags           NUMBER NOT NULL,

    PRIMARY KEY(user_id, stream_token)
);

CREATE TABLE IF NOT EXISTS read_markers (
    user_id         STRING NOT NULL,
    room_id         STRING NOT NULL,
    stream_token    NUMBER NOT NULL,

    PRIMARY KEY(user_id, room_id)
);
