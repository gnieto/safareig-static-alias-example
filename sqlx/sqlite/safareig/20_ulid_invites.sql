CREATE TABLE IF NOT EXISTS invites_tmp (
    user_id         STRING NOT NULL,
    stream_token    STRING NOT NULL,
    room_id         STRING NOT NULL,
    event_id        STRING,
    room_version    STRING,
    state           JSON
);

INSERT INTO invites_tmp(user_id, stream_token, room_id, event_id, room_version, state)
SELECT user_id, CAST(stream_token AS TEXT), room_id, event_id, room_version, state FROM invites;

DROP TABLE IF EXISTS invites;
ALTER TABLE invites_tmp RENAME TO invites;
