DROP INDEX IF EXISTS events_txn_id;

CREATE UNIQUE INDEX IF NOT EXISTS events_txn_id
    ON events (room_id, transaction_id);