DROP INDEX IF EXISTS states_unique_key;


CREATE TABLE IF NOT EXISTS states_temp (
    state_id            BLOB        NOT NULL,
    event_type          BLOB        NOT NULL,
    state_key           BLOB        NOT NULL,
    event_id            STRING      NOT NULL
);

INSERT INTO states_temp
SELECT state_id, event_type, COALESCE(state_key, x''), event_id FROM states;

DROP TABLE states;

ALTER TABLE states_temp RENAME TO states;

CREATE UNIQUE INDEX IF NOT EXISTS states_unique_key
    ON states (state_id, event_type, state_key);