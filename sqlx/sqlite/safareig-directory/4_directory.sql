CREATE TABLE IF NOT EXISTS public_rooms
(
    room_id         STRING      NOT NULL PRIMARY KEY,
    member_count    INTEGER     NOT NULL
);