DROP TABLE IF EXISTS public_rooms;

CREATE TABLE IF NOT EXISTS public_rooms
(
    room_id             STRING      NOT NULL PRIMARY KEY,
    canonical_alias     TEXT,
    name                TEXT,
    joined_members      INTEGER     NOT NULL,
    topic               TEXT,
    world_readable      NUMERIC     NOT NULL,
    guest_can_join      NUMERIC     NOT NULL,
    avatar_url          TEXT,
    join_rule           TEXT,
    federable           NUMERIC
);