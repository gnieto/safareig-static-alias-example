use std::{
    collections::HashMap,
    path::{Path},
    sync::Arc,
};


use safareig_appservices::{AppServiceConfig, Appservice, AppserviceRepository};
use safareig_authentication::auth::{AccessToken, TokenError, TokenHandler, UserTokens};
use safareig_core::{
    command::{Container, Module, Router, ServiceCollectionExtension},
    ddi::{ServiceCollectionExt, ServiceProvider},
    ruma::{
        DeviceId, OwnedDeviceId, OwnedRoomAliasId, OwnedRoomId, OwnedUserId, RoomAliasId, RoomId,
        UserId,
    },
    storage::StorageError,
};
use safareig_devices::storage::{Device, DeviceStorage};
use safareig_rooms::alias::storage::{AliasStorage, RoomAlias};
use serde::Deserialize;

pub struct StaticAliasModule {}

impl Default for StaticAliasModule {
    fn default() -> Self {
        Self {}
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for StaticAliasModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container
            .service_collection
            .service_factory(|_sp: &ServiceProvider| {
                let service: Arc<dyn TokenHandler> = Arc::new(NoopHandler::default());

                Ok(service)
            });

        container
            .service_collection
            .service_factory(|_sp: &ServiceProvider| {
                let service: Arc<dyn AppserviceRepository> = Arc::new(NoopAppservice::default());

                Ok(service)
            });

        container
            .service_collection
            .service_factory(|_sp: &ServiceProvider| {
                let service: Arc<dyn DeviceStorage> = Arc::new(NoopDeviceStorage::default());

                Ok(service)
            });

        container.service_collection.register::<Appservice>();

        // In-memory alias storage
        let storage: Arc<dyn AliasStorage> =
            Arc::new(TomlAliasStorage::new(&Path::new("./alias.toml")).unwrap());

        container.service_collection.service(storage);
    }
}

#[derive(Default)]
pub struct InMemoryAliasStorage;

#[async_trait::async_trait]
impl AliasStorage for InMemoryAliasStorage {
    async fn get_alias(&self, alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError> {
        match alias.to_string().as_str() {
            "#test:localhost:8228" => Ok(Some(RoomAlias {
                room_id: RoomId::new(alias.server_name()),
                creator: UserId::new(alias.server_name()),
                alias: alias.into(),
            })),
            _ => Ok(None),
        }
    }

    async fn room_aliases(&self, _room_id: &RoomId) -> Result<Vec<RoomAlias>, StorageError> {
        Ok(Vec::new())
    }

    async fn set_alias(
        &self,
        _room_alias: RoomAlias,
        _allow_replace: bool,
    ) -> Result<(), StorageError> {
        Ok(())
    }
    async fn remove_alias(&self, _alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError> {
        Ok(None)
    }
}

#[derive(Default)]
pub struct TomlAliasStorage {
    aliases: HashMap<OwnedRoomAliasId, RoomAliasData>,
}

#[derive(Deserialize)]
struct RoomAliasData {
    room_id: OwnedRoomId,
    owner: OwnedUserId,
}

impl TomlAliasStorage {
    pub fn new(path: &Path) -> anyhow::Result<Self> {
        let content = std::fs::read_to_string(path)?;
        let aliases: HashMap<OwnedRoomAliasId, RoomAliasData> = toml::from_str(&content)?;

        Ok(Self { aliases })
    }
}

#[async_trait::async_trait]
impl AliasStorage for TomlAliasStorage {
    async fn get_alias(&self, alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError> {
        let alias_data = self.aliases.get(alias).map(|data| RoomAlias {
            room_id: data.room_id.clone(),
            creator: data.owner.clone(),
            alias: alias.to_owned(),
        });

        Ok(alias_data)
    }

    async fn room_aliases(&self, _room_id: &RoomId) -> Result<Vec<RoomAlias>, StorageError> {
        Ok(Vec::new())
    }

    async fn set_alias(
        &self,
        _room_alias: RoomAlias,
        _allow_replace: bool,
    ) -> Result<(), StorageError> {
        Ok(())
    }

    async fn remove_alias(&self, _alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError> {
        Ok(None)
    }
}

#[derive(Default)]
pub struct NoopHandler;

#[async_trait::async_trait]
impl TokenHandler for NoopHandler {
    async fn generate_token(&self, _user_id: &UserId, _device_id: &DeviceId) -> UserTokens {
        unimplemented!("")
    }

    async fn parse_token(&self, _access_token: &str) -> Result<Box<dyn AccessToken>, TokenError> {
        Err(TokenError::ExpiredToken)
    }
}

#[derive(Default)]
pub struct NoopAppservice;

impl AppserviceRepository for NoopAppservice {
    fn get_by_id(&self, _id: &str) -> Option<&AppServiceConfig> {
        None
    }

    fn get_by_token(&self, _token: &str) -> Option<&AppServiceConfig> {
        None
    }

    fn all(&self) -> Vec<AppServiceConfig> {
        Vec::new()
    }
}

#[derive(Default)]
pub struct NoopDeviceStorage;

#[async_trait::async_trait]
impl DeviceStorage for NoopDeviceStorage {
    async fn get_device(
        &self,
        _user_id: &UserId,
        _device_id: &DeviceId,
    ) -> Result<Option<Device>, StorageError> {
        unimplemented!()
    }

    async fn devices_by_user(&self, _user_id: &UserId) -> Result<Vec<Device>, StorageError> {
        unimplemented!()
    }

    async fn update_device(&self, _device: &Device) -> Result<(), StorageError> {
        unimplemented!()
    }

    async fn delete_device(&self, _device: &Device) -> Result<(), StorageError> {
        unimplemented!()
    }

    async fn assign(
        &self,
        _auth: String,
        _user_id: &UserId,
        _device_id: &DeviceId,
    ) -> Result<(), StorageError> {
        unimplemented!()
    }

    async fn get_auth(
        &self,
        _auth: String,
    ) -> Result<Option<(OwnedUserId, OwnedDeviceId)>, StorageError> {
        unimplemented!()
    }

    async fn revoke_auth(&self, _auth: String) -> Result<(), StorageError> {
        unimplemented!()
    }
}
