use std::{env, path::PathBuf, sync::Arc};

use axum::Router;
use module::StaticAliasModule;
use safareig_axum::{adapters::AxumRouter, server::ServerBuilder, AxumModule};
use safareig_core::{
    app::{App, AppOptions},
    component::CoreModule,
    config::HomeserverConfig,
};
use safareig_federation::ServerKeysModule;
use safareig_rooms::alias::module::AliasesModule;

mod module;

#[tokio::main]
async fn main() {
    let config = load_config();
    let background = graceful::Background::default();

    let router = AxumRouter::new(Router::new());
    let options = AppOptions {
        spawn_worker: Some(background.handler()),
        run_migrations: true,
    };

    let app = App::new(router, options, config.clone())
        .with_module(Box::new(CoreModule::new(config.clone())))
        .with_module(Box::<ServerKeysModule>::default())
        .with_module(Box::<AliasesModule>::default())
        .with_module(Box::<StaticAliasModule>::default())
        .with_module(Box::<AxumModule>::default());

    let server = ServerBuilder::default()
        .with_cors(true)
        .with_tracing(true)
        .with_prometheus(true)
        .with_media_endpoints(true)
        .with_app(app)
        .build()
        .unwrap();

    server.run(background).await.unwrap();
}

pub fn load_config() -> Arc<HomeserverConfig> {
    let path = env::var("SAFAREIG_CONFIG").unwrap_or_else(|_| "Settings.toml".to_string());
    let cfg_file = PathBuf::from(&path);

    Arc::new(HomeserverConfig::from_file(cfg_file.as_ref()))
}
